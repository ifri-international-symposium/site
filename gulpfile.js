var gulp = require("gulp");
var browserSync = require("browser-sync").create();

gulp.task('default', function () {
  browserSync.init({
    server: {
      baseDir: ".",
      middleware: function (req, res, next) {
        res.setHeader('Access-Control-Allow-Origin', '*');
        next();
    }      
    },
  });
});
